"use strict";

/**
 * Country data service
 */
class Country
{
    /**
     * Get data from a country
     */
    constructor()
    {
        /**
         * REST countries API URI: \
         * `https://restcountries.eu/rest/v2/alpha`
         */
        this.api = 'http://restcountries.eu/rest/v2/alpha';

        this.http = require('http');
    }

    /**
     * Return a country data as an object
     * @param {string} country Country ISO3 code
     * @returns {object} Country data object
     */
    async getData(country)
    {
        return new Promise((resolve, reject) => {
            this.http.get(this.api + '/' + country, (res) => {
                country = '';

                res.on('data', (data) => {
                    country += data;
                });

                res.on('end', () => {
                    country = JSON.parse(country);

                    resolve(country);
                });
            }).on('error', (error) => {
                reject(error);
            });
        });
    }
}

module.exports = Country;
